from net import Net
import argparse
import models
import common

models_name = model_names = sorted(name for name in models.__dict__ if not name.startswith("__") and callable(models.__dict__[name]))

parser = argparse.ArgumentParser(description='PyTorch AlexNet Training')
parser.add_argument('-e', '--epochs', action='store', default=10, type=int, help='epochs (default: 10)')
parser.add_argument('-t', '--angle', action='store', default=1, type=int, help='Only load data within angle (default: 1)')
parser.add_argument('--batchSize', action='store', default=1, type=int, help='batch size (default: 128)')
parser.add_argument('--lr', '--learning-rate', action='store', default=0.01, type=float, help='learning rate (default: 0.01)')
parser.add_argument('--weight-decay', '--wd', default=1e-4, type=float, metavar='W', help='weight decay (default: 1e-4)')
parser.add_argument('--m', '--momentum', action='store', default=0.9, type=float, help='momentum (default: 0.9)')
parser.add_argument('--train_all_para_f', action='store_false', default=True, help='Flag to train all parameters (STORE_TRUE)(default: True)')
parser.add_argument('--gpu', action='store', default=1, type=int, help='Device ID (default: 1)')
parser.add_argument('--useGPU_f', action='store_false', default=True, help='Flag to use GPU (STORE_FALSE)(default: True)')
parser.add_argument('--preTrained_f', action='store_false', default=True, help='Flag to pretrained model (default: True)')
parser.add_argument('--stepSize', default=50)
parser.add_argument("--net", default='AlexNet', const='AlexNet',nargs='?', choices=models_name, help="net model(default:AlexNet)")
parser.add_argument("--train", default=None, const=None, nargs='?', choices=common.path.keys(), help="Training Set (default:ImageNet)")
parser.add_argument("--test" , default=None, const=None, nargs='?', choices=common.path.keys(), help="Testing Set (default:ImageNet)")
arg = parser.parse_args()

net = Net(arg)
net.train()