from common import *
from logger import logger

import torch
import numpy as np
import cv2
import torchvision
from torchvision import datasets, models
import os
import random
import datetime

class Net:
    def __init__(self, arg):
        if not os.path.exists('model'):
            os.makedirs('model')
        self.arg = arg
        self.net = arg.net
        self.train_path = path[arg.train]['train'] if arg.train else 'No Training Set'
        self.test_path = path[arg.test]['test'] if arg.test else 'No Testing Set'
        self.model_path = './model/' + datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S') + '.pt'
        self.logger = logger(self)
        
        self.__set_dataset__()
        self.__set_network__()
        
           
    def __set_dataset__(self):
        print 'Setting Dataset...'
        arg = self.arg
        
        self.image_datasets = {}
        self.dataloaders = {}
        self.dataset_sizes = {}
        if arg.train:
            self.image_datasets['train'] = datasets.ImageFolder(os.path.join(self.train_path),data_transforms['train'])
            self.dataloaders['train'] = \
                    torch.utils.data.DataLoader(self.image_datasets['train'], batch_size=arg.batchSize, shuffle=True, num_workers=4)
            self.dataset_sizes['train'] = len(self.image_datasets['train'])
        if arg.test:
            self.image_datasets['test'] = datasets.ImageFolder(os.path.join(self.test_path),data_transforms['test'])
            self.dataloaders['test'] = \
                    torch.utils.data.DataLoader(self.image_datasets['test'], batch_size=arg.batchSize, shuffle=True, num_workers=4)
            self.dataset_sizes['test'] = len(self.image_datasets['test'])
        
        assert arg.train or arg.test, 'Must Have One of Training and Testing'
        # number of classes should be 40 for modelnet
        if arg.train and arg.test:
            assert len(self.image_datasets['test'].classes)==len(self.image_datasets['train'].classes), 'Class Number Not Equal'
            self.classes = self.image_datasets['test'].classes
            self.class_num = len(self.classes)
        else:
            self.class_names = self.image_datasets['test'].classes if not arg.train else self.image_datasets['train'].classes
            self.class_num = len(self.class_names)
        
    def __set_network__(self):
        print 'Setting Network...'
        arg = self.arg
        model = models.__dict__[arg.net](self.class_num)
            
        if not arg.train_all_para_f:
            for name,param in model.named_parameters():
                param.requires_grad=False
                
        self.optimizer = torch.optim.SGD(filter(lambda p: p.requires_grad, model.parameters()), arg.lr,
                                momentum=arg.m,
                                weight_decay=arg.weight_decay)        
        if arg.useGPU_f:
            model.cuda(arg.gpu)
        
        #if os.path.isfile(self.model_path):
            #model.load_state_dict(torch.load(self.model_path))
        #model.eval()
        self.model = model
    
    def toVar(self, x):
        if self.arg.useGPU_f:
            return torch.autograd.Variable(x.cuda(self.arg.gpu)) 
        else:
            return torch.autograd.Variable(x)
    
    def train(self):
        arg = self.arg
        model = self.model
        logger = self.logger
        dataloaders = self.dataloaders
        optimizer = self.optimizer
        
        model.train()
        print("Start Training")
        logger.info("Start Training")
        epochs = arg.epochs
        for epoch in xrange(epochs):
            # trainning
            overall_acc = 0
            for batch_idx, (x, target) in enumerate(dataloaders['train']):
                
                optimizer.zero_grad()

                x, target = self.toVar(x), self.toVar(target)

                loss, pred_label = self.predict(x, target)
                
                correct = (pred_label == target.data).sum()
                overall_acc += correct
                accuracy = correct*1.0/arg.batchSize

                loss.backward()              
                optimizer.step()             
                
                if batch_idx%100==0:
                    print('==>>> epoch:{}, batch index: {}, train loss:{}, accuracy:{}'.format(epoch,batch_idx, loss.data[0], accuracy))
                    logger.info('==>>> epoch:{}, batch index: {}, train loss:{}, accuracy:{}'.format(epoch,batch_idx, loss.data[0], accuracy))

            # save the model per epochs
            torch.save(model.state_dict(), model_path)
    
    def test(self):
        arg = self.arg
        model = self.model
        logger = self.logger
        class_num = self.class_num
        dataloaders = self.dataloaders
        
        print("Start Testing")
        logger.info("Start Testing")
        if os.path.isfile(self.model_path):
            model.load_state_dict(torch.load(self.model_path))
            
        model.eval()
        correct, ave_loss = 0, 0
        correct_class = [0]*class_num
        count_class = [0]*class_num
        for batch_idx, (x, target) in enumerate(dataloaders['test']):
            
            x, target = self.toVar(x), self.toVar(target)

            loss, pred_label = self.predict(x, target)
            
            correct += (pred_label == target.data).sum()
            ave_loss += loss.data[0]
            for i in range(len(pred_label)):
                if pred_label[i] == target.data[i]:
                    correct_class[pred_label[i]] += 1
                count_class[target.data[i]] += 1



        accuracy = correct*1.0/dataset_sizes['test']
        ave_loss /= dataset_sizes['test']
        print('==>>> test loss:{}, accuracy:{}'.format(ave_loss, accuracy))
        logger.info('==>>> test loss:{}, accuracy:{}'.format(ave_loss, accuracy))
        for i in range(class_num):
            logger.info('==>>> class:{:10} accuracy:{}'.format(self.class_names[i], correct_class[i]*1.0/count_class[i]))
    
    def predict(self, x, target):   
        # use cross entropy loss
        criterion = torch.nn.CrossEntropyLoss()
        outputs = self.model(x)
        loss = criterion(outputs, target)
        _, pred_label = torch.max(outputs.data, 1)
        return loss, pred_label
        
    def torch2cv(self, x):
        img = Variable(x).data.cpu().numpy()
        img = img.squeeze()
        img = np.moveaxis(img, 0, -1)   
        return img
    def cv2torch(self, img):
        transformed_img = np.moveaxis(img, -1, 0)
        transformed_img = np.expand_dims(transformed_img, axis=0)
        return transformed_img
        