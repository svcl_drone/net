import logging
import os
import datetime

class logger(logging.Logger):
    def __init__(self, model):
        if not os.path.exists('log'):
            os.makedirs('log')
        # LOGGER Setting
        LOGGER = logging.getLogger('netlog')
        LOGGER.setLevel(logging.INFO)
        ch = logging.FileHandler('log/' + datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S') + '.log')
        ch.setLevel(logging.INFO)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        ch.setFormatter(formatter)
        LOGGER.addHandler(ch)
        LOGGER.info('==================================================================================')
        for attr, value in sorted(model.arg.__dict__.items()):
            text ="\t{}={}\n".format(attr.upper(), value)
            LOGGER.info(text)
            print text
        LOGGER.info('..................................................................................')
        LOGGER.info('Train Path: {}'.format(model.train_path))
        LOGGER.info('Test Path: {}'.format(model.test_path))
        LOGGER.info('Model Path: {}'.format(model.model_path))
        LOGGER.info('..................................................................................')
        self.logger = LOGGER
    def info(self, str):
        self.logger.info(str)