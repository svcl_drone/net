import torch.nn as nn
from torchvision.models.alexnet import model_urls as urls
import torchvision.models as models

__all__ = ['AlexNet']

class AlexNet(nn.Module):
    def __init__(self, num_classes=25):
        urls['alexnet'] = urls['alexnet'].replace('https://', 'http://')
        model = models.alexnet(pretrained=arg.preTrained_f)
        #Replace Lst FC Layer
        model.classifier._modules['6']= nn.Linear(4096, out_features=num_classes)
        self.model = model
        
    def forward(self, x):
        return self.model(x)