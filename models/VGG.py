import torch.nn as nn
from torchvision.models.vgg import model_urls as urls
import torchvision.models as models

__all__ = ['VGG']

class VGG(nn.Module):
    def __init__(self, num_classes=25):
        urls['vgg16'] = urls['vgg16'].replace('https://', 'http://')
        model = models.vgg16(pretrained=arg.preTrained_f)
        #Replace Lst FC Layer
        model.classifier._modules['6']= nn.Linear(4096, out_features=num_classes)
        