import torch.nn as nn
from torchvision.models.resnet import model_urls as urls
import torchvision.models as models

__all__ = ['ResNet18', 'ResNet34']

class ResNet18(nn.Module):
    def __init__(self, num_classes=25):
        urls['resnet18'] = urls['resnet18'].replace('https://', 'http://')
        model = models.resnet18(pretrained=arg.preTrained_f)
        #Replace Lst FC Layer
        model.fc= nn.Linear(512, out_features=num_classes)

class ResNet34(nn.Module):
    def __init__(self, num_classes=25):
        urls['resnet34'] = urls['resnet34'].replace('https://', 'http://')
        model = models.resnet34(pretrained=arg.preTrained_f)
        #Replace Lst FC Layer
        model.fc= nn.Linear(512, out_features=num_classes)