import torchvision
from torchvision import transforms

path = {'ModelNet':\
                    {'train':'/data5/drone_machinelearning/ModelNet/new_model_net/train',\
                     'test': '/data5/drone_machinelearning/ModelNet/new_model_net/test'},\
        'ImageNet':\
                    {'train':'/data/imgDB/DB/ILSVRC/2012/train', \
                     'test': '/data5/drone_machinelearning/imageNetData/val'},\
        'OverlappedImageNet':\
                    {'train':'/data5/drone_machinelearning/pose_modelnet/train', \
                     'test': '/data5/drone_machinelearning/overlapped_pose_modelnet/test'}
        }

data_transforms = {
    'train': transforms.Compose([
        transforms.RandomResizedCrop(224),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ]),
    'test': transforms.Compose([
        transforms.Resize(256),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ]),
}